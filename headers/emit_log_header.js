const amqplib = require("amqplib");

const exchangeName = "header_logs";
const args = process.argv.slice(2);
const msg = args[0] || "Hi! How are you?";

const sendMsg = async () => {
  const connection = await amqplib.connect("amqp://localhost");
  const channel = await connection.createChannel();
  await channel.assertExchange(exchangeName, "headers", { durable: false });

  channel.publish(exchangeName, "", Buffer.from(msg), {
    headers: { account: "new", method: "facebook" },
  });
  console.log("sent: ", msg);

  setTimeout(() => {
    connection.close();
    process.exit();
  }, 500);
};

sendMsg();
