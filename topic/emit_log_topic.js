const amqplib = require("amqplib");

const exchangeName = "topic_logs";
const args = process.argv.slice(2);
const msg = args[1] || "Hi! How are you?";
const logType = args[0];

const sendMsg = async () => {
  const connection = await amqplib.connect("amqp://localhost");
  const channel = await connection.createChannel();
  await channel.assertExchange(exchangeName, "topic", { durable: false });

  channel.publish(exchangeName, logType, Buffer.from(msg));
  console.log("sent: ", msg);

  setTimeout(() => {
    connection.close();
    process.exit();
  }, 500);
};

sendMsg();
