const Influx = require("influx");

const influx = new Influx.InfluxDB({
  host: "localhost",
  port: 8086,
  database: "devices",
});

module.exports = { influx };
