const amqplib = require("amqplib");

const queueName = "task";

const consumeTask = async () => {
  const connection = await amqplib.connect("amqp://localhost");
  const channel = await connection.createChannel();
  await channel.assertQueue(queueName, { durable: true });
  channel.prefetch(1);

  console.log(`Waiting for messages in queue: ${queueName}`);

  channel.consume(
    queueName,
    (msg) => {
      console.log(JSON.parse(msg.content));
    },
    { noAck: false }
  );
};

consumeTask();
