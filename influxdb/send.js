const amqplib = require("amqplib");
const { influx } = require("./influx");

const queueName = "task";

const getMsg = async (startTime, endTime, devID) => {
  const data = await influx.query(
    `SELECT * from ${devID} WHERE time >= ${startTime} AND time <= ${endTime} ORDER BY time`
  );
  return data;
};

const sendTask = async () => {
  const connection = await amqplib.connect("amqp://localhost");
  const channel = await connection.createChannel();
  await channel.assertQueue(queueName, { durable: true });

  const msg = await getMsg(1657532664878, 1657535896660, "INEM_DEMO");

  channel.sendToQueue(queueName, Buffer.from(JSON.stringify(msg)), {
    persistent: false,
  });
  console.log("sent: ", msg);

  setTimeout(() => {
    connection.close();
    process.exit();
  }, 500);
};

sendTask();
